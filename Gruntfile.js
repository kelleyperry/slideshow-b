// This shows a full config file!
module.exports = function(grunt) {
  grunt.initConfig({
    watch: {
      files: './sass/**/*.scss',
      tasks: ['sass']
    },
    sass: {
      dev: {
        options: {
          sourceMap: true,
          outputStyle: 'compressed'
        },
        files: {
          './css/styles.css': './sass/styles.scss'
        }
      }
    },
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            './css/*.css',
            './*.html'
          ]
        },
        options: {
          watchTask: true,
          server: './'
        }
      }
    }
  });

  // load npm tasks
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  // define default task
  grunt.registerTask('default', ['browserSync', 'watch']);
};
