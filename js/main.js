$(document).ready(function() {

  var slideWrapper = $(".slideshow");
  var slides = $(".slideshow ul");
  var slide = $(".slideshow ul li");
  var slideImg = $(".slideshow ul li img");

  var win = $(window);
  var btnLeft = $(".btns .left");
  var btnRight = $(".btns .right");

  win.load(function() {

    var slideWidth = slide.width();
    var slideNum = slide.length;
    var slidesWidth = slideWidth * slideNum;
    var slidesHeight = $(".slideshow ul li.current").innerHeight();

    slides.width(slidesWidth);
    slideWrapper.height(slidesHeight);
    slide.css({
      'width': slidesWidth / slideNum,
    });

    slide.addClass(function(index) {
      return "slide-" + (index + 1);
    });

    slide.each(function() {
      slideImgHeight = $(this).children("img").height();
      $(this).css("height", slideImgHeight);
    });

    win.resize(function(event) {

      var winWidth = $(window).width();
      var slideWidth = slide.width();
      var slideImgHeight = slideImg.height();

      if (winWidth <= slideWidth) {

        console.log('less than slide width');

        slideWidth = winWidth;


      } else if (winWidth > slideWidth) {

        console.log('greater than slide width');
        slideWidth = slide.width();

      }


      var slideNum = slide.length;
      var slidesWidth = slideWidth * slideNum;

      slides.width(slidesWidth);
      slide.css('width', slidesWidth / slideNum);
    });


    function slideMove(direction) {
      //disables the clicks ability to move
      //the slide until the previous animation is complete
      if (slides.is(':animated')) {
        return false;
      }
      //animate the slide
      slides.animate({
        left: direction + slideWidth
      }, "fast");
    }

    btnLeft.click(function(event) {

      var slidePos = slides.position();
      // slide strip moves to the right
      if (slidePos.left >= 0) {
        btnLeft.addClass("disabled");
      } else {
        slideMove("+=");
        btnLeft.removeClass("disabled");
      }
      if (btnLeft.hasClass('disabled')) {
        $(".slideshow ul li:first-child").addClass('current');
      } else {
        $(".slideshow ul li.current").removeClass('current').prev('li').addClass('current');
      }
    });

    btnRight.click(function(event) {

      var slidePos = slides.position();
      // slide strip moves to the left
      if (slidePos.left <= (-slidesWidth + slideWidth)) {
        btnRight.addClass("disabled");
      } else {
        slideMove("-=");
        btnRight.removeClass("disabled");
      }
      if (btnRight.hasClass('disabled')) {
        $(".slideshow ul li:last-child").addClass('current');
      } else {
        $(".slideshow ul li.current").removeClass('current').next('li').addClass('current');
      }
    });
  });
});
